package wk3;

import us.msoe.cs1011.Complex;

public class Driver {
    public static void main(String[] args) {
        Calculable num1 = createCalculable();
        Calculable num2 = createCalculable();
        num2 = new Complex(2);
        System.out.println(num1 + " + " + num2 + " = " + num1.plus(num2));
        System.out.println(num1 + " - " + num2 + " = " + num1.minus(num2));
        System.out.println(num1.equals(num2));
    }

    public static Calculable createCalculable() {
        //return new Double(Math.random());
        return new Double(2);
    }
}
