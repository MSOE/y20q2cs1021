package wk3;

public class Double extends Calculable {
    private final double value;

    public Double(double value) {
        this.value = value;
    }

    @Override
    public Calculable plus(Calculable that) {
        return new Double(this.value + that.doubleValue());
    }

    @Override
    public Calculable minus(Calculable that) {
        return new Double(this.value - that.doubleValue());
    }

    @Override
    public double doubleValue() {
        return value;
    }

    @Override
    public int intValue() {
        return (int)Math.round(value);
    }

    @Override
    public boolean equals(Object that) {
        boolean isEqual = false;
        if(that instanceof Calculable) {
            Calculable temp = (Calculable)that;
            isEqual = value==temp.doubleValue();
        }
        return isEqual;
    }

    @Override
    public String toString() {
        return "" + value;
    }
}
