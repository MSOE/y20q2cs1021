package wk3;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class GUI extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    private Circle circle;

    @Override
    public void start(Stage stage) {
        Group group = new Group();
        Scene scene = new Scene(group, 200, 150);
        circle =new Circle(60, 40, 30, Color.GREEN);
        Text text = new Text(10, 90, "JavaFX Scene");
        Button bigger = new Button("Bigger");
        bigger.setOnAction(this::buttonClick);
        group.getChildren().add(circle);
        group.getChildren().add(text);
        group.getChildren().add(bigger);

        stage.setTitle("Title bar");
        stage.setScene(scene);
        stage.show();
    }

    private void buttonClick(ActionEvent event) {
        circle.setRadius(1.1*circle.getRadius());
    }
}
