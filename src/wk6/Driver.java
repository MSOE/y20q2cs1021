package wk6;

import java.io.*;
import java.util.Scanner;

public class Driver {
    public static void main(String[] args) {
        try (Scanner dis = new Scanner(new FileInputStream("test2.png"))) {
            System.out.println(dis.next().length());
            System.out.println(dis.next());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void main3(String[] args) {
        {
            DataOutputStream fos = null;
            try {
                fos = new DataOutputStream(new FileOutputStream("test2.png"));
                fos.writeDouble(Math.PI);
                fos.write(97);
                fos.write(0b00110011);
                fos.write('M');
                fos.write('S');
                fos.write('O');
                fos.write('E');
            } catch (FileNotFoundException e) {
                System.out.println("Could not find the directory where you wanted to" +
                        " dump this file");
            } catch (IOException e) {
                System.out.println("Problems writing the file. So sorry");
            } finally {
                try {
                    if (fos != null) {
                        fos.close();
                    }
                } catch (IOException e) {
                    System.out.println("Big problems, can't even close the file");
                }
            }
        }
    }

    public static void main2(String[] args) throws NumberFormatException, NullPointerException {
        System.out.println("Happy exception day");
        try {
            if("a".length()>0) {
                throw new EOFException("too bad");
            }
            System.out.println("dksk");
        } catch(EOFException e) {

        } catch(IndexOutOfBoundsException e) {

        } catch(NumberFormatException | NullPointerException e) {
            if(e instanceof NumberFormatException) {
                System.out.println("You are not so good with numbers");
            }
        }
    }
}
