package wk1;

import java.util.Scanner;

public class Excited {
    public static void main(String[] args) {
        Class1 c = new Class1();
        System.out.println(c.toString());
        // v = m; // legal since v is more general than m
        // m = v; // illegal since v is less general than m
        // m = (MountainBike)v; // legal if v is a m
        //Scanner in = new Scanner(System.in);
        //System.out.println("Enter a line of text");
        //String word = in.nextLine();
        //if(word.length()>=7 && word.charAt(6)<='m') {
        //    System.out.println("The seventh character is in the first half of the alpha");
        //}
        //System.out.println("Did you hear about the circus fire?");
        //System.out.println(decrypt("Vg jnf va gragf."));
        //System.out.println(decrypt(decrypt("abcdefghijklmno...z"+(char)('A'-1) + "AB...Z")));
    }

    public static String decrypt(String phrase) {
        String decryptedPhrase = "";
        for(int i=0; i<phrase.length(); i++) {
            char character = phrase.charAt(i);
            if(Character.isLetter(character)) {
                decryptedPhrase += rotate(character);
            } else {
                decryptedPhrase += character;
            }
        }
        return decryptedPhrase;
    }

    private static char rotate(char letter) {
        char rotatedChar = (char)(letter + 13);
        if(Character.toLowerCase(letter)>'m') {
            rotatedChar = (char)(letter - 13);
        }
        return rotatedChar;
    }
}
