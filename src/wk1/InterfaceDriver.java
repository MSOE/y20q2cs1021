package wk1;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class InterfaceDriver {
    public static void main(String[] args) {
        List<String> numbers = new LinkedList<>();
        for(int i=0; i<5; i++) {
            numbers.add("" + i);
        }
        for(int i=0; i<numbers.size(); i++) {
            numbers.set(i, numbers.get(i) + numbers.get(i));
        }
        System.out.println(numbers);
    }

    public static void main2(String[] args) {
        Interface1 ref1 = new Class2();
        if(Math.random()<0.5) {
            ref1 = new Class1();
        }
        System.out.println(ref1.generate());
    }
}
