package wk4;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class Controller {

    @FXML
    private Label greeting;

    public void handleExit(ActionEvent actionEvent) {
        Platform.exit();
    }
}
