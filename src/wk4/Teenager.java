package wk4;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
import javafx.stage.Stage;

public class Teenager extends Application {

    private Label response;
    private TextField input;
    public static final int HEIGHT = 200;
    public static final int WIDTH = 300;
    @Override
    public void start(Stage stage) {
        response = new Label();
        input = new TextField();
        input.setOnAction(this::handleInput);
        Button clear = new Button("Clear");
        clear.setOnAction(event -> {
            response.setText("");
            input.setText("");
        });
        GridPane pane = new GridPane();
        pane.add(new Label("Please enter your age: "), 0, 0);
        pane.add(input, 0, 1);
        pane.add(response, 1, 2);
        pane.add(clear, 2, 0);
        stage.setScene(new Scene(pane, WIDTH, HEIGHT));
        stage.show();
    }

    private void handleInput(ActionEvent event) {
        String text = ((TextField)event.getSource()).getText();
        int age = Integer.parseInt(text);
        String not = "";
        if(age<13 || age>19) {
            not = "not ";
        }
        response.setText("You are " + not + "a teenager.");
    }

    public static void main(String[] args) {
        launch(args);
    }

}
