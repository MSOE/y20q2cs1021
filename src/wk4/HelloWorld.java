package wk4;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class HelloWorld extends Application {

    @Override
    public void start(Stage stage) {
        Label label = new Label("Hello World!");
        label.setFont(new Font(40));
        Button exitButton = new Button("Exit");
        FlowPane pane = new FlowPane();
        pane.getChildren().addAll(label, exitButton);
        Scene scene = new Scene(pane);
        stage.setTitle("Welcome to JavaFX!");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
