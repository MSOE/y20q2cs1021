package wk8;

import java.io.IOException;
import java.util.IntSummaryStatistics;
import java.util.List;

public class SectionDriver {
    private static final String FILENAME = "taylorSections.txt";

    public static void main(String[] args) {
        try {
            Loader loader = new Loader(FILENAME);
            List<Section> sections = loader.getSections();
        } catch (IOException e) {
            System.err.println("Unable to load file: " + FILENAME);
        }
    }

}
