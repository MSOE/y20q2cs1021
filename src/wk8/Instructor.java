package wk8;

import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

public class Instructor implements Comparable<Instructor> {
    private final String name;
    private final int id;
    private Set<Section> sections;

    public boolean addSection(Section section) {
        return sections.add(section);
    }

    public Instructor(int id, String name) {
        sections = new TreeSet<>();
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return name + ", " + id +
                "\n Sections: " + sections;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Instructor that = (Instructor) o;
        return id == that.id && Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, id);
    }

    @Override
    public int compareTo(Instructor that) {
        int result = name.compareTo(that.name);
        if(result==0) {
            result = id-that.id;
        }
        return result;
    }
}
