package wk8;

import java.util.List;
import java.util.Objects;

public class Course implements Comparable<Course> {
    private static Course dummy;

    private final String courseCode;
    private final String courseTitle;
    private final int credits;
    private int lectureHours;
    private int labHours;
    private final String type;
    private final String department;
    private List<Course> coreqs;
    private List<Course> prereqs;
    private List<Course> equivalents;

    public Course(String code, String title, String type, String department, int lecHours, int labHours, int credits) {
        this.courseCode = code;
        this.courseTitle = title;
        this.type = type;
        this.department = department;
        this.lectureHours = lecHours;
        this.labHours = labHours;
        this.credits = credits;
    }

    public String getCourseCode() {
        return courseCode;
    }

    @Override
    public String toString() {
        return courseCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Course course = (Course) o;
        return courseCode.equals(course.courseCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(courseCode);
    }

    @Override
    public int compareTo(Course that) {
        return courseCode.compareTo(that.courseCode);
    }

    public static Course getDummy() {
        if(dummy==null) {
            dummy = new Course("NOCODE", "Not a course", "", "", 0, 0, 0);
        }
        return dummy;
    }
}
