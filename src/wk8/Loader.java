package wk8;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;

public class Loader {
    private Map<Integer, Instructor> instructors;
    private Map<String, Course> courses;
    private List<Section> sections;

    private static final List<String> COL_LOOKUP = Arrays.asList("Year", "Term",
            "CourseSection", "-", "Credits", "Capacity", "Enrollment", "CourseCode",
            "-", "Section", "Type", "Title", "-", "Coreqs", "Prereqs", "Status",
            "InstructorId", "InstructorLast", "InstructorFirst", "Department",
            "InstructorName", "LectureHours", "LabHours");
    private static final int YEAR = COL_LOOKUP.indexOf("Year");
    private static final int TERM = COL_LOOKUP.indexOf("Term");
    private static final int CREDITS = COL_LOOKUP.indexOf("Credits");
    private static final int CAPACITY = COL_LOOKUP.indexOf("Capacity");
    private static final int ENROLLMENT = COL_LOOKUP.indexOf("Enrollment");
    private static final int COURSE_CODE = COL_LOOKUP.indexOf("CourseCode");
    private static final int SECTION = COL_LOOKUP.indexOf("Section");
    private static final int TYPE = COL_LOOKUP.indexOf("Type");
    private static final int TITLE = COL_LOOKUP.indexOf("Title");
    private static final int COREQS = COL_LOOKUP.indexOf("Coreqs");
    private static final int PREREQS = COL_LOOKUP.indexOf("Prereqs");
    private static final int STATUS = COL_LOOKUP.indexOf("Status");
    private static final int INSTRUCTOR_ID = COL_LOOKUP.indexOf("InstructorId");
    private static final int INSTRUCTOR_NAME = COL_LOOKUP.indexOf("InstructorName");
    private static final int DEPARTMENT = COL_LOOKUP.indexOf("Department");
    private static final int LEC_HOURS = COL_LOOKUP.indexOf("LectureHours");
    private static final int LAB_HOURS = COL_LOOKUP.indexOf("LabHours");

    public Loader(String filename) throws IOException {
        instructors = new TreeMap<>();
        courses = new TreeMap<>();
        sections = new ArrayList<>();

        Path file = Paths.get(filename);
        Stream<String> in = Files.lines(file);
        in.forEach(this::processRecord);
    }

    private void processRecord(String record) {
        String[] fields = record.split("\t");
        String code = fields[COURSE_CODE];
        String title = fields[TITLE];
        String type = fields[TYPE];
        String department = fields[DEPARTMENT];
        int lecture = Integer.parseInt(fields[LEC_HOURS]);
        int lab = Integer.parseInt(fields[LAB_HOURS]);
        int credits = Integer.parseInt(fields[CREDITS]);
        Course course = courses.get(code);
        if(course==null) {
            course = new Course(code, title, type, department, lecture, lab, credits);
            courses.put(code, course);
        }

        int year = Integer.parseInt(fields[YEAR]);
        String term = fields[TERM];
        String sectionNumber = fields[SECTION];
        int capacity = Integer.parseInt(fields[CAPACITY]);
        int enrollment = Integer.parseInt(fields[ENROLLMENT]);
        boolean isFull = fields[STATUS].equals("F");
        Section section = new Section(year, term, course, sectionNumber, capacity, enrollment, isFull);
        sections.add(section);

        int id = Integer.parseInt(fields[INSTRUCTOR_ID]);
        String name = fields[INSTRUCTOR_NAME];
        Instructor instructor = instructors.get(id);
        if(instructor==null) {
            instructor = new Instructor(id, name);
            instructors.put(id, instructor);
        }
        instructor.addSection(section);
    }

    public Map<Integer, Instructor> getInstructors() {
        return instructors;
    }

    public Map<String, Course> getCourses() {
        return courses;
    }

    public List<Section> getSections() {
        return sections;
    }
}
