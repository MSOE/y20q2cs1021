package wk8;

import java.util.Objects;

public class Section implements Comparable<Section> {
    private final int year;
    private final String quarter;
    private Course course;
    private final String sectionNumber;
    private final int capacity;
    private final int enrollment;
    private final boolean isFull;

    public Section(int year, String term, Course course, String sectionNumber, int capacity,
                   int enrollment, boolean isFull) {
        this.year = year;
        this.quarter = term;
        this.course = course;
        this.sectionNumber = sectionNumber;
        this.capacity = capacity;
        this.enrollment = enrollment;
        this.isFull = isFull;
    }

    @Override
    public int compareTo(Section that) {
        int result = year-that.year;
        if(result==0) {
            result = quarter.compareTo(that.quarter);
        }
        if(result==0) {
            result = sectionNumber.compareTo(that.sectionNumber);
        }
        return result;
    }

    @Override
    public String toString() {
        return "Y" + year + " " + quarter +
                " " + course +
                "-" + sectionNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Section section = (Section) o;
        return year == section.year &&
                quarter.equals(section.quarter) &&
                course.equals(section.course) &&
                sectionNumber.equals(section.sectionNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(year, quarter, course, sectionNumber);
    }

    public Course getCourse() {
        return course!=null ? course : Course.getDummy();
    }

    public int getYear() {
        return year;
    }

    public String getQuarter() {
        return quarter;
    }

    public String getSectionNumber() {
        return sectionNumber;
    }

    public int getCapacity() {
        return capacity;
    }

    public int getEnrollment() {
        return enrollment;
    }

    public boolean isFull() {
        return isFull;
    }
}
