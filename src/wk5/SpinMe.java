package wk5;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class SpinMe extends Application {

    private final static int WIDTH = 300;
    private final static int HEIGHT = 200;
    private Button spinningButton;
    private int angle;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) {
        angle = 0;
        BorderPane pane = new BorderPane();
        spinningButton = new Button("Exit");
        Button clockwiseButton = new Button("Rotate Clockwise");
        Button counterClockwiseButton = new Button("Rotate CounterClockwise");
        pane.setCenter(spinningButton);
        pane.setRight(clockwiseButton);
        pane.setLeft(counterClockwiseButton);
        spinningButton.setOnAction(event -> Platform.exit());
        clockwiseButton.setOnAction(this::rotateClockwise);
        counterClockwiseButton.setOnAction(this::rotateCounterClockwise);
        pane.setAlignment(clockwiseButton, Pos.CENTER);
        pane.setAlignment(counterClockwiseButton, Pos.CENTER);
        stage.setScene(new Scene(pane, WIDTH, HEIGHT));
        stage.show();
    }

    private void rotateCounterClockwise(ActionEvent actionEvent) {
        angle = (angle-10)%360;
        spinningButton.setRotate(angle);
    }

    private void rotateClockwise(ActionEvent actionEvent) {
        angle = (angle+10)%360;
        spinningButton.setRotate(angle);
    }
}
