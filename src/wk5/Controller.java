package wk5;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextInputDialog;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

import java.util.Optional;

public class Controller {
    @FXML
    private Button spinningButton;
    private int angle = 0;

    public void handleExit(ActionEvent actionEvent) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure you want to exit?");
        alert.showAndWait().ifPresent(response -> {
            if (response == ButtonType.OK) {
                Platform.exit();
            }
        });
//        Optional<ButtonType> result = alert.showAndWait();
//        if (result.isPresent() && result.get() == ButtonType.OK) {
//            Platform.exit();
//        }
    }

    @FXML
    public void rotateClockwise(ActionEvent actionEvent) {
        TextInputDialog inputDialog = new TextInputDialog("10");
        inputDialog.showAndWait().ifPresent(input -> {
            try {
                angle = (angle + Integer.parseInt(input)) % 360;
            } catch (NumberFormatException e) {
                System.out.println("You blew it, using 10 degrees");
                angle = (angle + 10) % 360;
                throw e;
            }
            finally {
                System.out.println("Always runs");
            }
            spinningButton.setRotate(angle);
        });
    }

    public void rotateCounterClockwise(ActionEvent actionEvent) {
        angle = (angle-10)%360;
        spinningButton.setRotate(angle);
    }

    public void help(MouseEvent mouseEvent) {
        System.out.println("help me more");
    }

    public void handleKeyPressed(KeyEvent keyEvent) {
        System.out.println(keyEvent.getText());
    }

    public void handleClick(MouseEvent mouseEvent) {
        System.out.println(mouseEvent.getX() + ", " + mouseEvent.getSceneX() + ", " + mouseEvent.getScreenX());
    }
}
