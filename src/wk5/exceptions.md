# Exceptions
## Keywords
* `try` - Pay attention to any exceptions that occur inside the code block
* `catch` - Handle exception thrown inside try block
* `finally` - Block of code that runs after the try block regardless of whether
an exception was thrown, caught, or not caught
* `throw` - Causes an exception to be thrown